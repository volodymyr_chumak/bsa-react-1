import PropTypes from 'prop-types';
import './styles.css';
import React from "react";
import generateUUID from "../../helpers/uuid-generator";

const MessageInput = ({onMessageAdd, onEditedMessageSave, editingMessage}) => {
    const [body, setBody] = React.useState('');
    const [editBody, setEditBody] = React.useState('');

    const handleMessageAdd = () => {
        if (!body) return;

        onMessageAdd({ id: generateUUID(), text: body, createdAt: new Date() });
        setBody('');
    };

    const handleEditedMessageSave = () => {
        if (!editBody) return;

        const updatedMessage = editingMessage;
        updatedMessage.text = editBody;
        updatedMessage.editedAt = new Date();

        onEditedMessageSave(updatedMessage);
        setEditBody('');
    };

    return (
        <>
            {editingMessage.text === "" ? (
                <div className="message-input">
                    <textarea
                    className="message-input-text"
                    value={body}
                    placeholder="Write a message ..."
                    onChange={ev => setBody(ev.target.value)}
                    />
                    <button onClick={handleMessageAdd} className="message-input-button">Send</button>
                </div>
            ) : (
                <div className="message-input">
                    <textarea
                    className="message-input-text"
                    value={editBody}
                    onChange={ev => setEditBody(ev.target.value)}
                    />
                    <button onClick={handleEditedMessageSave} className="message-input-button">Update</button>
                </div>
            )}
        </>
    );
}

MessageInput.propTypes = {
    onMessageAdd: PropTypes.func.isRequired,
    onEditedMessageSave: PropTypes.func.isRequired,
    editingMessage: PropTypes.object.isRequired
}

export default MessageInput;