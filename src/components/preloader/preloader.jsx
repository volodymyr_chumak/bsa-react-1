import PropTypes from 'prop-types';
import './styles.css';

const Preloader = ({ isFetching }) => {
    return (
        <div className="preloader">
            { isFetching ? (<img src="spinner.svg"  alt="" />) : ""}
        </div>
    );
};

Preloader.propTypes = {
    isFetching: PropTypes.bool.isRequired
}

export default Preloader;