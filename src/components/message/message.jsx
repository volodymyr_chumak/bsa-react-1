import PropTypes from 'prop-types';
import './styles.css';
import dateFormat from 'dateformat';
import React from "react";

const Message = ({ message, isLiked, onMessageLike}) => {
    const {
        id,
        avatar,
        user,
        text,
        createdAt
    } = message;

    const creationTime = dateFormat(createdAt, "HH:MM");

    const handleMessageLike = () => {
        onMessageLike(id);
    }

    return (
        // className={`messsage${isLiked ? ' message-liked' : ''}`}>  --- такий варіант теж не проходив тест
        <div className={`${isLiked ? 'message-liked' : 'message'}`}>
            <img className="message-user-avatar" src={avatar} alt="avatar" />
            <p className="message-user-name">{user}</p>
            <p className="message-time">{creationTime}</p>
            <p className="message-text">{text}</p>
            <button className="message-like" onClick={handleMessageLike}>Like</button>
        </div>
    );
}

Message.propTypes = {
    message: PropTypes.object.isRequired,
    isLiked: PropTypes.bool.isRequired,
    onMessageLike: PropTypes.func.isRequired
}

export default Message;