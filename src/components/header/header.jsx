import PropTypes from 'prop-types';
import dateFormat from 'dateformat';
import './styles.css';

const Header = ({ messages }) => {
    const messagesCount = messages.length;
    const usersCount = countUsers(messages);
    const lastMessageDate = getLastMessageDateAsString(messages);

    return (
        <div className="header">
            <div className="header-title">
                <p>My chat</p>
            </div>
            <div className="header-users-count">
                <p>{usersCount}</p>
            </div>
            <div className="header-messages-count">
                <p>{messagesCount}</p>
            </div>
            <div className="header-last-message-date">
                <p>{lastMessageDate}</p>
            </div>
        </div>
    );
}

const countUsers = messages => {
    if (messages.length === 0) return 0;
    const userIds = new Set();
    messages.map(message => userIds.add(message.userId));

    return userIds.size;
}

const getLastMessageDateAsString = messages => {
    if (messages.length === 0) return "";
    const dates = messages.map(message => new Date(message.createdAt));
    dates.sort((date1, date2) => date1 - date2);

    return dateFormat(dates[messages.length-1], "dd.mm.yyyy HH:MM");
}

Header.propTypes = {
    messages: PropTypes.array.isRequired
}

export default Header;