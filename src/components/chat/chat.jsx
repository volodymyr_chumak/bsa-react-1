import React from "react";
import Preloader from "../preloader/preloader";
import Header from "../header/header";
import './styles.css';
import MessageList from "../message-list/message-list";
import MessageInput from "../message-input/message-input";
import generateUUID from "../../helpers/uuid-generator";
import PropTypes from "prop-types";
import {AuthUserId} from "../../common/enums/auth-user-id.enum";

class Chat extends React.Component {
    state = {
        messages: [{ createdAt: "2020-07-16T19:48:12.936Z"}],
        isFetching: true,
        editingMessage: {text: ""},
        authUserId: AuthUserId.NEW_USER, // Змініть це поле на AuthUserId.USER_WITH_MESSAGES, якщо хочете зайти як існуючий користувач
        likes: new Map()
    }

    componentDidMount() {
        this.loadMessages(this.props.url);
    }

    loadMessages(url) {
        fetch(url)
            .then(response => response.json())
            .then(data => {
                this.setState({ messages: data });
                this.setState({ isFetching: false });
            });
    }

    handleMessageAdd = message => {
        let authUser = this.getAuthUserId();
        const avatar = this.getAvatarOfAuthUser();
        const user = this.getUsernameOfAuthUser();

        const oldState = this.state.messages;
        oldState.push({
            id: message.id,
            userId: authUser,
            avatar: avatar,
            user: user,
            text: message.text,
            createdAt: message.createdAt,
            editedAt: ""
        });

        this.setState({ messages: oldState})
    }

    mockMessage = {
        id: AuthUserId.USER_WITH_MESSAGES,
        avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
        user: "Ruth"
    }

    getAuthUserId = () => {
        if (this.state.authUserId === this.mockMessage.id) return this.mockMessage.id;
        if (this.state.authUserId !== AuthUserId.NEW_USER) return this.state.authUserId;

        const generatedId = generateUUID();
        this.setState({authUserId: generatedId})
        return generatedId;
    }

    getAvatarOfAuthUser = () => {
        return this.state.authUserId === this.mockMessage.id ? this.mockMessage.avatar : "";
    }

    getUsernameOfAuthUser = () => {
        return this.state.authUserId === this.mockMessage.id ? this.mockMessage.user : `user-${generateUUID()}`;
    }

    handleMessageEdit = message => {
        this.setState({ editingMessage: message})
    }

    handleEditedMessageSave = message => {
        const oldMessages = this.state.messages;
        const updatedMessages = oldMessages.filter(m => m.id !== message.id);
        updatedMessages.push(message);

        this.setState({ messages: updatedMessages});
        this.setState({ editingMessage: {text: ""}});
    }

    handleMessageDelete = id => {
        const oldMessages = this.state.messages;
        const updatedMessages = oldMessages.filter(m => m.id !== id);

        this.setState({ messages: updatedMessages})
    }

    handleMessageLike = id => {
        const oldState = this.state.likes;
        const isLiked = Boolean(oldState.get(id));
        oldState.set(id, !isLiked);

        this.setState(oldState);
    }

    render() {
        return (
            <div className="chat">
                <Header messages={this.state.messages} />
                <MessageList
                    messages={this.state.messages}
                    likes={this.state.likes}
                    onMessageEdit={this.handleMessageEdit}
                    onMessageDelete={this.handleMessageDelete}
                    authUserId={this.state.authUserId}
                    onMessageLike={this.handleMessageLike}
                />
                <MessageInput
                    messages={this.state.messages}
                    editingMessage={this.state.editingMessage}
                    onMessageAdd={this.handleMessageAdd}
                    onEditedMessageSave={this.handleEditedMessageSave}
                />
                <Preloader isFetching={this.state.isFetching} />
            </div>
        );
    }
}

Chat.propTypes = {
    url: PropTypes.string.isRequired
}

export default Chat;