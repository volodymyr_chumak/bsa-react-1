import PropTypes from 'prop-types';
import './styles.css';
import Message from "../message/message";
import OwnMessage from "../own-message/own-message";
import React, {Fragment} from "react";
import { formatDateOnly, formatDateOnlyLong } from '../../helpers/date-format';
import countDaysFromPastToPresentDate from "../../helpers/days-between";


const MessageList = ({ messages, likes, onMessageEdit, onMessageDelete, authUserId, onMessageLike }) => {
    const sorterMessages = sortMessages(messages);

    let lastDate;

    const isNeedDivider = message => {
        const currentDate = formatDateOnly(new Date(message.createdAt));
        let isNeedDivider = false;
        if (currentDate !== lastDate) {
            isNeedDivider = true;
            lastDate = currentDate;
        }
        return isNeedDivider;
    }

    return (
        <div className="message-list">
            {sorterMessages.map(message => {
                const isLiked = Boolean(likes.get(message.id));

                const divider = isNeedDivider(message)
                    ? (<div
                        key={lastDate}
                        className="messages-divider">
                        {getDividerLabel(new Date(message.createdAt))}
                        </div>)
                    : <Fragment key={`withoutDivider-${message.id}`} />;

                const msg = message.userId === authUserId
                    ? (<OwnMessage
                        key={message.id}
                        message={message}
                        onMessageEdit={onMessageEdit}
                        onMessageDelete={onMessageDelete}
                        />)
                    : (<Message key={message.id} message={message} isLiked={isLiked} onMessageLike={onMessageLike} />);

                return (
                    <Fragment key={`messageFragment-${message.id}`}>
                        { divider }
                        { msg }
                    </Fragment>
                );
            })
            }
        </div>
    );
}

const sortMessages = messages => {
    return [...messages].sort((m1, m2) => new Date(m1.createdAt) - new Date(m2.createdAt));
}

const getDividerLabel = (currentDate) => {
    const formattedCurrentDate = formatDateOnly(currentDate);
    const formattedDateNowDate = formatDateOnly(new Date());
    if (formattedCurrentDate === formattedDateNowDate) return 'Today';

    const daysBetween = countDaysFromPastToPresentDate(currentDate);
    if (daysBetween === 1) return 'Yesterday';

    return formatDateOnlyLong(currentDate);
}

MessageList.propTypes = {
    messages: PropTypes.array.isRequired,
    likes: PropTypes.object.isRequired,
    onMessageEdit: PropTypes.func.isRequired,
    onMessageDelete: PropTypes.func.isRequired,
    onMessageLike: PropTypes.func.isRequired
}

export default MessageList;