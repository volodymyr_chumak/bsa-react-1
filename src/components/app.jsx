import Chat from "./chat/chat";

function app() {
    return (
        <div className="App">
            <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
        </div>
    );
}

export default app;
