import PropTypes from 'prop-types';
import './styles.css';
import dateFormat from 'dateformat';

const OwnMessage = ({ message, onMessageEdit, onMessageDelete }) => {
    const {
        id,
        text,
        createdAt
    } = message;

    const creationTime = dateFormat(createdAt, "HH:MM");

    const handleMessageEdit = () => onMessageEdit(message);
    const handleMessageDelete = () => onMessageDelete(id);

    return (
        <div className="own-message">
            <p className="message-time">{creationTime}</p>
            <p className="message-text">{text}</p>
            <button className="message-edit" onClick={handleMessageEdit}>Edit</button>
            <button className="message-delete" onClick={handleMessageDelete}>Delete</button>
        </div>
    );
}

OwnMessage.propTypes = {
    message: PropTypes.object.isRequired,
    onMessageEdit: PropTypes.func.isRequired,
    onMessageDelete: PropTypes.func.isRequired
}

export default OwnMessage;