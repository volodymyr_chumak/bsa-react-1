import dateFormat from "dateformat";

const formatDateOnly = date => dateFormat(date, "dd.mm.yyyy");

const formatDateOnlyLong = date => dateFormat(date, "dddd, dd mmmm");

export { formatDateOnly, formatDateOnlyLong };