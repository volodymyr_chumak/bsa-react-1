const countDaysFromPastToPresentDate = pastDate => {
    return Math.floor((new Date().getTime() - pastDate.getTime()) / (1000*60*60*24));
}

export default countDaysFromPastToPresentDate;